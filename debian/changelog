abook (0.6.1-3) unstable; urgency=low

  * The DebConf23 cleanup upload.
  * Update fix_spelling_error_in_binary.diff to also include it.po and ja.po.
    ALso, in the debian/rules clean target get rid of the po/*.gmo files and
    the stamp file (closes: #1043710)
  * Bump Standards-Version to 4.6.2.
  * Update Build-Depends package names.
  * Add Rules-Requires-Root: no.
  * Update copyright year.

 -- Rhonda D'Vine <rhonda@debian.org>  Wed, 06 Sep 2023 17:37:09 +0530

abook (0.6.1-2) unstable; urgency=low

  [ Denis Briand ]
  * Bump standards version to 4.6.0
  * Fix SIGSEGV with undefined USER environment variable (Closes: #859407).

  [ Bastian Germann ]
  * Complete debian/copyright (Closes: #995843, #979098)

  [ Rhonda D'Vine ]
  * Bump debhelper-compat to 13.
  * Move VCS to salsa.
  * Convert debian/po/{de,fr,sv}.po to utf8.
  * Bump watchfile version to 4.

 -- Rhonda D'Vine <rhonda@debian.org>  Sat, 27 Nov 2021 11:14:49 +0100

abook (0.6.1-1) unstable; urgency=medium

  [ Rhonda D'Vine ]
  * New upstream release, which fixes:
    + Add an address to Abook with Mutt delete all phone numbers
      (Closes: #727245)
    + incorporated patches 02_fix-manpage (Closes: #771343)


  [ Denis Briand ]
  * Use dh_autoreconf.
  * Remove autotools-dev build dependency.
  * Remove merged patch 03_datafile-f-switch.
  * Add debhelper token into maintainers scripts.
  * Rename postinst and postrm files into abook.*
  * Switch debian/copyright file to machine readable format.
  * Add fix_spelling_error_in_binary.diff patch
    to fix error with "occurred" word

 -- Denis Briand <debian@denis-briand.fr>  Wed, 30 Dec 2015 03:45:10 +0100

abook (0.6.0~pre2-5) unstable; urgency=low

  * fix error into debian/postinst file (Closes: #791803).
    Thanks to Salvatore Bonaccorso.
  * Switch source package to debhelper.
  * Fix broken Vcs-git link (Closes: #791612)

 -- Denis Briand <debian@denis-briand.fr>  Thu, 13 Aug 2015 16:48:16 +0200

abook (0.6.0~pre2-4) unstable; urgency=medium

  [ Rhonda D'Vine ]
  * Add -std=gnu89 to CFLAGS as suggested by Martin Michlmayr to fix FTBFS
    with gcc-5, thanks (closes: #777764)

  [ Denis Briand ]
  * Add me as co-maintainer (Thanks Rhonda).
  * Bump standards version to 3.9.6
  * Use dpkg-buildflags.
  * Rename Debian version (same of upstream tarball)
  * Switch to dpkg-source 3.0 (quilt) format

 -- Denis Briand <debian@denis-briand.fr>  Mon, 06 Jul 2015 15:13:16 +0200

abook (0.6.0~pre2-3) unstable; urgency=low

  * Overlooked NMU done by Tim Retout while merging from experimental to
    unstable, resulting in (repeated) FTBFS:
    - debian/control: Build-Depend on libreadline6-dev instead of
      libreadline5-dev. (Closes: #553712)
  * Applied that again (closes: #670445)

 -- Gerfried Fuchs <rhonda@debian.org>  Thu, 26 Apr 2012 00:04:58 +0200

abook (0.6.0~pre2-2) unstable; urgency=low

  * Finally uploading to unstable.  Making sure the changes are carried over,
    especially:
    - from 0.5.6-2:
      - Swedish debconf translation, already mentioned in changelog but without
        closes: #387518
      - Fixed quoting in mutt config snippet noticed by Georg Neis, thanks
        (closes: #394532)
    - from 0.5.6-3:
      - Japanese debconf translation by Noritada Kobayashi (closes: #412998)
    - from 0.5.6-4:
      - Dutch debconf translation by Bart Cornelis (closes: #415513)
      - Updated menu file to go with new menu policy.
      - debian/rules: slightly cleanup.
    - from 0.5.6-5:
      - Add Homepage to source control stanca.
      - Bump Standards-Version to 3.7.3, menu file was already updated.
      - Russian debconf translation by Yuri Kozlov (closes: #451813)
      - search_ctrl-d_segfault_fix included in the release already
        (closes: #462145)
    - from 0.5.6-6:
      - Made copyright file more clear.
      - Updated watch file to use special uscan hack for sourceforge.
      - Updated Standards-Version to 3.8.0, added README.source file.
      - New debconf translations:
        - Spanish debconf translation by Carlos Eduardo Sotelo Pinto
          (closes: #484848)
        - Portuguese debconf translation, already mentioned in changelog but
          without (closes: #489036). Actually the one mentioned was a newer
          one than the one in the unstable branch.
      - Fixed Catalan po file encoding.
      - Use versioned Build-Depends on quilt for quilt.make usage.
      - Also strip sections .comment and .note
      - Add patch fix-manpage to escape hyphens where they should be minus
        signs (adjusted it for the 0.6.0~pre2 release)
    - from 0.5.6-7:
      - Build-Depend on autotools-dev, copy config.{guess,sub} before configure
        run and remove them in clean target (closes: #535739)
      - Updated to Standards-Version 3.8.2: Switched from findstring to filter
        and add support parallel.
        suggestion from policy.
      - Switched the packaging licensing to BSD style.
      - New patch datafile-f-switch: Let --datafile also work through -f short
        option, suggested by Joey Schulze
      - Traditional Chinese debconf translation by Kanru Chen (closes: #508023)
  * Relicense packaging under WTFPLv2.
  * Bump Standards-Version to 3.9.3.
  * New debconf translations:
    - Danish by Joe Hansen (closes: #626630)
    - Polish by Michał Kułach (closes: #658602)
    - Italian by Francesca Ciceri (closes: #658744)
  * Add Vcs-* fields to debian/control.
  * Explicit call set -e in config/postrm/postinst scripts instead of on the
    shebang line.
  * Add recommended targets build-arch/build-indep to debian/rules.

 -- Gerfried Fuchs <rhonda@debian.org>  Tue, 24 Apr 2012 19:59:54 +0200

abook (0.6.0~pre2-1) experimental; urgency=low

  * New upstream release

 -- Gerfried Fuchs <alfie@debian.org>  Thu,  7 Sep 2006 10:29:20 -0500

abook (0.6.0~pre1-1) experimental; urgency=low

  * Upstream pre version of 0.6.0 release.
  * new debconf translations: French by Pierre Machard (closes: #384681)

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 31 Aug 2006 07:26:11 -0500

abook (0.5.6+cvs1-1) experimental; urgency=low

  * CVS snapshot with flexible view, which resolves:
    - small number of email addresses (closes: #333450)
  * new debconf translations: Portuguese by Miguel Figueiredo, Català by
    Jordà Polo (closes: #381362), Swedish by Daniel Nylander
  * Fixed typo in template, noticed by Pierre Machard (closes: #383473)
  * contrib files got deleted from upstream, so they are removed from the
    Debian package, too.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 21 Aug 2006 07:41:58 -0500

abook (0.5.6-1) unstable; urgency=low

  * New upstream release.
  * Updated config.{guess,sub}.
  * Bumped Standards-Version to 3.7.2, no changes needed.
  * Used debconf for mutt query inclusion instead of outdated upgrade note
    that would only affect upgrades from oldstable (woody) (closes: #377633).
  * new debconf translations: Romanian by Eddy Petrişor (closes: #380505),
    Czech by Miroslav Kure, Brazilian Portuguese by Felipe Augusto van de
    Wiel, Vietnamese by Clytie Siddall, Hungarian by Attila Szervñc, German by
    myself.

 -- Gerfried Fuchs <alfie@debian.org>  Wed, 02 Aug 2006 09:15:46 -0500

abook (0.5.5-1) unstable; urgency=low

  * New upstream release.
  * New debconf translation:
    - Spanish by César Gómez Martín (closes: #333758)
  * Applied patch to fix some incorrect entries in the help screen.
  * Updated config.sub and config.guess.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 12 Dec 2005 12:35:38 +0100

abook (0.5.4-3) unstable; urgency=low

  * Added alternative for debconf-2.0 (closes: #331730)
  * Removed article from synopsis.
  * New debconf translation:
    - Swedish by Daniel Nylander (closes: #331292)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 10 Oct 2005 13:17:09 +0200

abook (0.5.4-2) unstable; urgency=low

  * The "lintian bugs R us" release:
    - Moved menu file from /usr/lib/menu to /usr/share/menu.
    - Removed bashism in postrm and config.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 05 Sep 2005 12:33:49 +0200

abook (0.5.4-1) unstable; urgency=low

  * New upstream release (closes: #314611)
  * Bumped standards version, no changes needed.
  * New debconf translation:
    - Vietnamese by Clytie Siddall (closes: #313019)
    - Italian by Luca Bruno (closes: #316911)
  * Changed Build-Depends from libreadline4-dev to
    libreadline5-dev | libreadline-dev (closes: #326392)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 05 Sep 2005 12:17:19 +0200

abook (0.5.3-3) unstable; urgency=high

  * New debconf translation:
    - Finnish by Matti Pöllä (closes: #303820)
    - Vietnamese by Clytie Siddall (closes: #307595)

 -- Gerfried Fuchs <alfie@debian.org>  Tue, 10 May 2005 12:42:58 +0200

abook (0.5.3-2) unstable; urgency=low

  * Updated config.sub and config.guess (closes: #299212)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 14 Mar 2005 14:38:39 +0100

abook (0.5.3-1) unstable; urgency=medium

  * New upstream release: fixes import from mutt aliases (closes: #254969)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 11 Oct 2004 10:19:14 +0200

abook (0.5.2-3) unstable; urgency=medium

  * Switched to libncursesw5-dev instead to support utf8 properly
    (closes: #271626)

 -- Gerfried Fuchs <alfie@debian.org>  Fri, 17 Sep 2004 16:02:51 +0200

abook (0.5.2-2) unstable; urgency=low

  * Updated watch file -- hope it works longer this time.
  * New debconf translations:
    - Catalan by Josep Monés i Teixidor (closes: #247151)
    - Czech by Ondra Kudlik (closes: #260615)

 -- Gerfried Fuchs <alfie@debian.org>  Mon,  3 May 2004 17:26:28 +0200

abook (0.5.2-1) unstable; urgency=low

  * New upstream release:
    - five custom fields added (closes: #158428)
    - abookrc manual page update (closes: #231909)
  * debian/menu: Quoted all the values

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 19 Apr 2004 20:27:48 +0200

abook (0.5.1-2) unstable; urgency=low

  *  Added patch from Jaakko Heinonen to fix segfault in memory handling
     (closes: #226581)
  * Two new po-debconf translations:
    - Japanese done by Hideki Yamane (closes: #227389)
    - Danish done by Morten Brix Pedersen (closes: #227789)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 26 Jan 2004 13:53:34 +0000

abook (0.5.1-1) unstable; urgency=low

  * New upstream release (closes: #224901)

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 25 Dec 2003 17:12:51 +0000

abook (0.5.0-4) unstable; urgency=low

  * Added Portuguese po-debconf translation,
    done by Bruno Rodrigues (closes: #216257)

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 30 Oct 2003 11:15:32 +0000

abook (0.5.0-3) unstable; urgency=low

  * Added po-debconf translations:
    - Dutch by Michiel Sikkes (closes: #214001)
    - Russian by Ilgiz Kalmetev (closes: #214353)
    Thanks to you all.

 -- Gerfried Fuchs <alfie@debian.org>  Mon,  6 Oct 2003 08:52:34 +0000

abook (0.5.0-2) unstable; urgency=low

  * Added po-debconf translations:
    - Polish by Bartosz Zapalowski (closes: #208953)
    - Frensh by Pierre Machard (closes: #205120)
    - Brazilian Portuguese by Andre Luis Lopes (closes: #205838)
  * Updated to policy 3.6.1 (no changes needed).

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 22 Sep 2003 10:42:44 +0000

abook (0.5.0-1) unstable; urgency=low

  * New upstream release (closes: #201836)
  * Updated to policy 3.6.0.
  * Build-Depends on libreadline4-dev now (closes: #136230, #163268)
    To get the old data simply press cursor up.
  * Added debconf hint message that old addressbook must be imported and
    can be deleted afterwards, location and format change.
  * de-debhelper-ized the package: DEB_BUILD_OPTIONS noopt and nostrip should
    be supported now, too.
  * removed emacs cruft from end of this file.
  * Add vim script into binary distribution, to contrib directory.

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 07 Aug 2003 23:49:52 +0200

abook (0.4.17-1) unstable; urgency=low

  * New maintainer.
  * New upstream release (closes: #131309) which closes: #152905
  * debian/control:
    -) Removed fullstop from short description.
    -) Updated to policy 3.5.9
  * debian/rules:
    -) moved compat level into debian/compat
    -) moved example files into debian/examples
    -) removed comented out dh_ lines that never will make it into the package
  * debian/menu:
    -) added longtitle
  * added debian/watch file

 -- Gerfried Fuchs <alfie@debian.org>  Fri, 28 Mar 2003 11:52:20 +0100

abook (0.4.16-1) unstable; urgency=low

  * New upstream release
  * Closes: #129867, #133482

 -- Alan Ford <alan@whirlnet.co.uk>  Sun, 17 Mar 2002 14:51:50 +0000

abook (0.4.15-1) unstable; urgency=low

  * New upstream release. Closes: #131309
  * Added some contrib files in /usr/share/doc/abook/

 -- Alan Ford <alan@whirlnet.co.uk>  Sun,  4 Nov 2001 14:50:38 +0000

abook (0.4.14-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Sat, 29 Sep 2001 22:49:00 +0100

abook (0.4.13-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Wed, 20 Jun 2001 22:16:42 +0100

abook (0.4.12-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Tue, 20 Mar 2001 21:51:22 +0000

abook (0.4.11-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Tue,  2 Jan 2001 17:08:46 +0000

abook (0.4.10-1) unstable; urgency=low

  * New upstream release
  * Fixes segfault bug. Closes: #70083

 -- Alan Ford <alan@whirlnet.co.uk>  Sat, 16 Sep 2000 09:52:19 +0100

abook (0.4.9-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Thu, 27 Jul 2000 23:00:07 +0100

abook (0.4.8-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Wed, 21 Jun 2000 16:33:48 +0100

abook (0.4.7-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Sun, 28 May 2000 16:10:59 +0100

abook (0.4.6-1) unstable; urgency=low

  * New upstream release
  * Makes fields less US-specific. Closes: #62797

 -- Alan Ford <alan@whirlnet.co.uk>  Thu, 11 May 2000 20:03:40 +0100

abook (0.4.5-1) unstable; urgency=low

  * New upstream release
  * Removed generic INSTALL from doc directory. Closes: #62789

 -- Alan Ford <alan@whirlnet.co.uk>  Fri, 21 Apr 2000 11:50:59 +0100

abook (0.4.4-1) unstable; urgency=low

  * New upstream release
  * Fixes segfault bug. Closes: #62022

 -- Alan Ford <alan@whirlnet.co.uk>  Sun,  9 Apr 2000 10:39:06 +0100

abook (0.4.3-1) unstable; urgency=low

  * New upstream release

 -- Alan Ford <alan@whirlnet.co.uk>  Sun,  2 Apr 2000 12:06:23 +0100

abook (0.4.2-1) unstable; urgency=low

  * Initial Release.

 -- Alan Ford <alan@whirlnet.co.uk>  Sat, 18 Mar 2000 12:38:34 +0000
